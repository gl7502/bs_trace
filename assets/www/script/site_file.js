var hetulab;

if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.site_file = {};

(function(){
	var ns = hetulab.bstrace.site_file;
	var dirHelper;
	
	var siteFileName;
	var siteFileWriter;
	
	function getRootName(fileSystem) {
// console.log(fileSystem.name);
// console.log(fileSystem.root.name);
		// 取得 root
		var rootEntry = fileSystem.root;

		rootEntry.getDirectory("helper", {
			create : true,
			exclusive : false
		}, function(dir) {
// console.log("zzzzzzzzzzzzz dirHelper1" + dir.name);
			dirHelper = dir;
			fileOp.dirHelper = dirHelper;
			dirHelper.getDirectory("site", {
				create : true,
				exclusive : false
			}, function(dir) {
// console.log("zzzzzzzzzzzzz dirSite " + dir.name);
				dirSite = dir;
				fileOp.dirSite = dirSite;

				// 创建 site trace 文件
				var now = new Date();
				var siteName = "site" + now.getTime() + ".txt";
				dirSite.getFile(siteName, {
					create : true,
					exclusive : false
				}, function(file) {
// console.log("zzzzzzzzzzzzz siteName" + file.name);
					file.createWriter(function(writer) {
// console.log("zzzzzzzzzzzzz siteFileWriter" + writer);
						siteFileWriter = writer;
					}, fileFail);
				}, fileFail);
			}, fileFail);
	});
		
	var fileFail = function(error) {
			alert(error.code);
	}	
	
})()