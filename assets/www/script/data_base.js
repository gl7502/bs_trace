var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.DATABASE = {};

(function() {
	var db ;
	
	document.addEventListener("deviceready", function(){
		db = initializeDB();
		createLocationTable(db);
	}, false);
	

	// open the database
	function initializeDB() {
		var localDatabase = openDatabase("tracer", // short name
		"1.0", // version
		"basestation information", // long name
		5000000 // maximum size in bytes
		);
		return localDatabase;
	}

	function createLocationTable(db) {
		var query = "CREATE TABLE IF NOT EXISTS bs_location ( "
		// location
		+ "lac_cid VARCHAR(50), " + "latitude REAL, " + "longitude REAL, "
				+ "altitude REAL, " + "accuracy REAL, "
				+ "altitude_accuracy REAL, "
				// address
				+ "street_number VARCHAR(50), " + "street VARCHAR(50), "
				+ "postal_code VARCHAR(50), " + "city INTEGER, "
				+ "county VARCHAR(50), " + "region VARCHAR(50), "
				+ "country INTEGER, " + "country_code INTEGER " + ")";
		db.transaction(function(trxn) {
			trxn.executeSql(query, // the query to execute
			[], // parameters for the query
			function(transaction, resultSet) { // success				
//				console.log('success');
			}, function(transaction, error) { // error callback
//				console.log(error);
			});
		});
	}

	function _insertBsLocation(db, lac,cid, loc) {
		var key = lac + "_" + cid;
		var query = "INSERT OR REPLACE INTO bs_location"
				+ "(lac_cid,latitude, longitude,altitude,accuracy,altitude_accuracy,"
				+ "street_number,street,postal_code,city,county,region,country,country_code) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?); "
		db.transaction(function(trxn) {
			trxn.executeSql(query, [ key, loc.location.latitude,
					loc.location.longitude, loc.location.altitude,
					loc.location.accuracy, loc.location.altitude_accuracy,
					loc.location.address.street_number,
					loc.location.address.street,
					loc.location.address.postal_code,
					loc.location.address.city, loc.location.address.county,
					loc.location.address.region, loc.location.address.country,
					loc.location.address.country_code ], function(transaction,
					resultSet) {
//				console.log('success');
			}, function(transaction, error) {
//				console.log(error);
			});
		});
	}
	
	function insertBsLocation( lac,cid, loc){
		_insertBsLocation(db, lac,cid, loc);
	}

	function _getBsLocation(db, lac,cid,success,fail) {
		var key = lac + "_" + cid;
		var query = "SELECT * FROM bs_location  where lac_cid=key;"
		db.transaction(function(trxn) {
			trxn.executeSql(query, // the query to execute
			[], // parameters for the query
			function(transaction, resultSet) {
//				var i = 0, currentRow, stories = [];
//				for (i; i < resultSet.rows.length; i++) {
//					currentRow = resultSet.rows.item(i);
//					stories.push(currentRow);
//				}
				if(resultSet.rows.length > 0){
//					console.log("-----_getBsLocation-success-lac---" + lac);
					success(lac,cid,resultSet.rows.item(0))
				} 

			}, function(transaction, error) { // error callback
//				console.log("-----_getBsLocation-fail-lac---" + lac);
//				console.log(error);
				fail(lac,cid,error);
			});
		});
	}
	
	function getBsLocation( lac,cid, success,fail){
		_getBsLocation(db, lac,cid,success,fail);
	}

	
	var ns = hetulab.bstrace.DATABASE;
	ns.insertBsLocation = insertBsLocation;
	ns.getBsLocation = getBsLocation;
		
})()