var hetulab;

(function() {
	var fileOp = hetulab.bstrace.fileoperate;
	var lock_manage = hetulab.bstrace.lock_manager;
	var infor_show = hetulab.bstrace.infor_show;

	var unlock_src = "../images/icons/unlock50-60.jpg";
	var lock_src = "../images/icons/lock50-60.jpg";
	var alert_enable = "../images/icons/sound50-60.jpg";
	var alert_disable = "../images/icons/disable50-60.jpg";

	$(document).on("pagecreate", "#page_lock_manage", function() {
		// 注册锁定按钮点击事件
		$("#lock_list_tbl").on("click", "td", function(evt) {
			infor_show.clickItem(this);
			showLockList(); // 及时刷新当前的更改
		}); // function(evt) {

	})

	$(document).on("pageshow", "#page_lock_manage", function() {
		showLockList();
	});
	
	function showLockList() {
		var locklist = lock_manage.getLockList();
		// console.log("------page lock showLockList ----" + locklist);
		// console.log("------page lock showLockList ----" + locklist.length);

		$("#lock_list_tbl").empty();
		$("#lock_list_tbl_head").empty();

		if (locklist.length == 0) {
			$("#noTable").show();
			$("#lock_bs_table").hide();
			$("#noTable").html("当前没有锁定的基站！")
			return;
		}

		$("#noTable").hide();
		$("#lock_bs_table").show();
		$("#lock_list_tbl_head").append(infor_show.creatInforHeader());

		for ( var i = 0; i < locklist.length; i++) {
			if(locklist[i].lock_status == 0) {
				locklist.splice(i,1);
				continue;
			}
			$("#lock_list_tbl").prepend(infor_show.creatInforLine(locklist[i]));
		}
	}

})()
