/**
 * 
 * @return Instance of CellMessage
 */
var CellMessage = function() {
}
/**
 * @param name
 *            The name passed in
 * @paramsuccessCallback The callback that will be called when simple plugin
 *                       runs successfully
 * @paramfailureCallback The callback that will be called when simple plugin
 *                       fails
 */
CellMessage.prototype.getCurBs = function(successCallback, failureCallback) {
	PhoneGap.exec(successCallback, // Success Callback
	failureCallback, // Failure Callback
	'CellMessage', // Registered Plug-in name
	'getCurBs', // Action
	[]); // Argument passed in
};

CellMessage.prototype.getPhoneType = function(successCallback, failureCallback) {
	PhoneGap.exec(successCallback, // Success Callback
	failureCallback, // Failure Callback
	'CellMessage', // Registered Plug-in name
	'getPhoneType', // Action
	[]); // Argument passed in
};

CellMessage.prototype.getLuce = function(successCallback, failureCallback) {
	PhoneGap.exec(successCallback, // Success Callback
	failureCallback, // Failure Callback
	'CellMessage', // Registered Plug-in name
	'getLuce', // Action
	[]); // Argument passed in
};

/**
 * <ul>
 * <li>Register the Simple Listing Javascript plugin.</li>
 * </ul>
 */
PhoneGap.addConstructor(function() {
	// Register the Javascript plug-in with PhoneGap
	PhoneGap.addPlugin('CellMessage', new CellMessage());
});