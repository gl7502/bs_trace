var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.internet = {};

(function() {
	var ns = hetulab.bstrace.internet;
	var db = hetulab.bstrace.DATABASE;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;
	
	var onlineStatus;
	
	document.addEventListener('deviceready', function() {
		document.addEventListener("online", onOnline, false);
		document.addEventListener("offline", onOffline, false);
	},true);
	
	function onOnline() {
	    // Handle the online event
		onlineStatus = true;
	}
	
	function onOffline(){
		// Handle the offline event
		onlineStatus = false;
	}
	
	function getOnlineStatus(){
		return onlineStatus;
	}

    function getNetWorkType(){
    	if(navigator.network && navigator.network.connection){
    		return navigator.network.connection.type;
    	}    	
    }
    
    function getNetWorkTypeStatus(){
     var networkState = navigator.network.connection.type;
	 var states = {};
     states[Connection.UNKNOWN]  = 'Unknown connection';
     states[Connection.ETHERNET] = 'Ethernet connection';
     states[Connection.WIFI]     = 'WiFi connection';
     states[Connection.CELL_2G]  = 'Cell 2G connection';
     states[Connection.CELL_3G]  = 'Cell 3G connection';
     states[Connection.CELL_4G]  = 'Cell 4G connection';
     states[Connection.NONE]     = 'No network connection';   
     
     return states[networkState];
    }
    
    function hasNetWork(){ 
    	if(navigator.network && navigator.network.connection){
            var networkState = navigator.network.connection.type;
            if(networkState == Connection.NONE || networkState == Connection.UNKNOWN){
            	return false;
            }else{
            	return true;
            }
    	}    
    }
    
    function showNoNetworkWarning(){
		if(!hasNetWork()){			
			alert("该操作需要网络连接，当前网络链接没有打开。将无法正常操作。");
		}
    }
    
    //向 google 请求 lac cid 基站 的位置
	function requestLocation(lac,cid,callback) {
		var cb = callback;
		var mcc = hetulab.bstrace.CELL_MESSAGE_INFOR.getMcc();
		var mnc = hetulab.bstrace.CELL_MESSAGE_INFOR.getMnc();

//		console.log("-----operator and name ----" + mcc + "," + mnc);

		var reqObj = {
			"version" : "1.1.0",
			"host" : "maps.google.com",
			
		    "home_mobile_country_code": mcc,
		    "home_mobile_network_code":mnc,
		    "radio_type": "gsm",
		    "request_address": true,
		    "address_language": "zh_CN",
		    
			"cell_towers" : [ {
				"cell_id" : cid,
				"location_area_code" : lac,
				"mobile_country_code" : mcc,
				"mobile_network_code" : mnc,
				"age" : 0,
				"signal_strength" : -10,
				"timing_advance" : 5555
			} ]
		}
		
//		console.log("---reqObj----" + JSON.stringify(reqObj));
		
		$.post(
				'http://www.google.com/loc/json', JSON.stringify(reqObj),
				function(data) {

					cb(lac,cid,data);
			});
	}
	

	
	ns.onOnline = onOnline;
	ns.onOffline = onOffline;
	ns.getOnlineStatus = getOnlineStatus;
	ns.getNetWorkType = getNetWorkType;
	ns.getNetWorkTypeStatus = getNetWorkTypeStatus;
	ns.hasNetWork = hasNetWork;	
	ns.showNoNetworkWarning = showNoNetworkWarning;
	ns.requestLocation = requestLocation;
	
})()

// 位置查询响应 的返回值
//{
//  "location": {
//    "latitude": 51.0,
//    "longitude": -0.1,
//    "altitude": 30.1,
//    "accuracy": 1200.4,
//    "altitude_accuracy": 10.6,
//    "address": {
//      "street_number": "100",
//      "street": "Amphibian Walkway",
//      "postal_code": "94043",
//      "city": "Mountain View",
//      "county": "Mountain View County",
//      "region": "California",
//      "country": "United States of America",
//      "country_code": "US"
//    }
//  },
//  "access_token": "2:k7j3G6LaL6u_lafw:4iXOeOpTh1glSXe"
//}