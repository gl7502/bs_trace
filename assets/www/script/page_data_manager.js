var hetulab;

(function() {
	var fileOp = hetulab.bstrace.fileoperate;
	
	var fileList;
	var selectFileNumber ;
	
	$(document).on("pagecreate", "#page_data_manager", function() {
		//fileOp.init();
		$("#list_view").click(function(evt){
			evt.preventDefault();
			if(fileList.length == 0){return true;}
			var checkedValue = $("input[type='radio']:checked").attr("value");
			console.log("-----list_view--" + checkedValue);
			fileOp.setShowFile(fileList[parseInt(checkedValue)]);
		})	
		
		$("#map_view").click(function(evt){
			evt.preventDefault();
			if(fileList.length == 0){return true;}
			var checkedValue = $("input[type='radio']:checked").attr("value");
			console.log("-----list_view--" + checkedValue);
			fileOp.setShowFile(fileList[parseInt(checkedValue)]);
		})			
		
		$("#file_delete").click(function(evt){
			evt.preventDefault();
			if(fileList.length == 0){return true;}
			selectFileNumber = parseInt($("input[type='radio']:checked").attr("value"));
			
			console.log("-----list_view--" + selectFileNumber);
			// PhoneGap Notification 提供的 Confirm API
			navigator.notification.confirm(
			    '真的要删除文件 '+fileList[selectFileNumber].name + ' 吗？',  // message
			     onDeleteConfirm,            // callback function
			     '删除文件',               // title
			     '是的,取消'              // confirm 選項，用逗號隔開
			    );

		});
		
		$("#file_delete_all").click(function(evt){
			evt.preventDefault();	
			if(fileList.length == 0){return true;}
			navigator.notification.confirm(
				    '真的要删除所有文件 吗？',  // message
				    onDeleteAllConfirm,            // callback function
				     '删除文件',               // title
				     '是的,取消'              // confirm 選項，用逗號隔開
				    );
		});			
	});
	
	$(document).on("pagebeforeshow", "#page_data_manager", function() {
		fileOp.getFileList(requestFileListCallBack);			
	});
	
	// callback function
	function onDeleteConfirm(button) {		     
	    // if press 'Yes'
	    if (button === 1){
//	    	console.log("-----onDeleteConfirm--" + fileOp);
			fileOp.remove(fileList[selectFileNumber]);
//			console.log("-----list_view fileList.length before--" + fileList.length);
			fileList.splice(selectFileNumber,1);
//			console.log("-----list_view fileList.length--" + fileList.length);

			updateFileList(fileList);			       
		    }
	}
	
	// callback function
	function onDeleteAllConfirm(button) {		     
	    // if press 'Yes'
	    if (button === 1){
			for(var i=0;i<fileList.length;i++){
//				console.log("-----list_view--" + i);
				fileOp.remove(fileList[i]);		
			}
//			console.log("-----list_view fileList.length before--" + fileList.length);
			fileList.splice(0);

			updateFileList(fileList);    
		}
	}
	
	function requestFileListCallBack(entries){
		
		fileList = entries;
		
		updateFileList(entries);
//		$("#file_name_list").empty();
//		var fileNameList = getSiteFileNameList(entries);
//		console.log("-----page_data_manager---" + fileNameList.length);
//		
//		for(var i = 0; i < fileNameList.length;i++){
//			var check = $("<input type='radio' name='filename'/>");
//			check.attr("value",i);
//			check.attr("id",i);
//			if(i == 0) check.attr("checked","checked");
//			
//			var label = $("<label></label>");
//			label.attr("for",i);
//			label.html(fileNameList[i]);
//			
//			$("#file_name_list").append(check).append(label);
//		}
//		$("#file_name_list").trigger('create');
	}
	
	function updateFileList(entries){
		$("#file_name_list").empty();
		var fileNameList = getSiteFileNameList(entries);
		console.log("-----page_data_manager---" + fileNameList.length);
		
		for(var i = 0; i < fileNameList.length;i++){
			var check = $("<input type='radio' name='filename'/>");
			check.attr("value",i);
			check.attr("id",i);
			if(i == 0) check.attr("checked","checked");
			
			var label = $("<label></label>");
			label.attr("for",i);
			label.html(fileNameList[i]);
			
			$("#file_name_list").append(check).append(label);
		}
		$("#file_name_list").trigger('create');
	}
	
	function getSiteFileNameList(fileList){
		var fileNameList = [];
	    for (var i=0; i<fileList.length; i++) {
	    	fileNameList[i] = fileList[i].name;
//			console.log("-----requestFileListCallBack-i--" + fileNameList[i] + ' ' + i);
	    }
	    return fileNameList;
	}
	

		
})();