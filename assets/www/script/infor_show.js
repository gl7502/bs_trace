var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.infor_show = {};

(function() {
	var lock_manage = hetulab.bstrace.lock_manager;
	var ns = hetulab.bstrace.infor_show;

	var unlock_src = "../images/lockmanage/unlock.png";
	var lock_src = "../images/lockmanage/lock.png";
	var alert_enable = "../images/lockmanage/sound2.png";
	var alert_disable = "../images/lockmanage/no_sound2.png";
	
	function clickItem(tab) {
		var lac_cid_val = $(tab).siblings(".lac_cid").html()
		var infors = lac_cid_val.splite("-");
		var lacTemp = parseInt(infors[0]);
		var cidTemp = parseInt(infors[1]);

		if ($(tab).attr("class") == "button_in") {
//			 console.log("----this--button in---");
			var newStatus = lock_manage.changeInLockStatus(lacTemp, cidTemp);

			// 依据改变后的状态调整显示
			if (newStatus) {
				$(tab).children().attr("src", lock_src);
			} else {
				$(tab).children().attr("src", unlock_src);
			}
		} else if ($(tab).attr("class") == "button_out") {
//			 console.log("----this--button  out---");
			var newStatus = lock_manage.changeOutLockStatus(lacTemp, cidTemp);

			// 依据改变后的状态调整显示
			if (newStatus) {
				$(tab).children().attr("src", lock_src);
			} else {
				$(tab).children().attr("src", unlock_src);
			}
		} else if ($(tab).attr("class") == "button_alert") {
			var newStatus = lock_manage.changeAlertStatus(lacTemp, cidTemp);
			console.log("----infor_show clickItem changeAlertStatus newStatus-----" + newStatus);

			// 依据改变后的状态调整显示
			if (newStatus) {
				$(tab).children().attr("src", alert_enable);
			} else {
				$(tab).children().attr("src", alert_disable);
			}
		}
	}; // function(evt) {

	function creatInforHeader() {
		var newline = $("<tr style='background-color: #A42D00'></tr>").append(
				"<th scope='col' style='color: white'>LAC-CID</th>").append(
				"<th scope='col' style='color: white'>进入锁</th>").append(
				"<th scope='col' style='color: white'>离开锁</th>").append(
				"<th scope='col' style='color: white'>告警</th>")
		return newline;
	}

	// 返回一个 $ 变量，表示基站对象的一行
	function creatInforLine(bsObj) {
		var inStatus = unlock_src;
		var outStatus = unlock_src;
		var alertStatus = alert_disable;

		if (bsObj.alert == 1) {
			alertStatus = alert_enable;
		}

		switch (bsObj.lock_status) {
		case 1:
			inStatus = lock_src;
			break;
		case 2:
			outStatus = lock_src;
			break;
		case 3:
			inStatus = lock_src;
			outStatus = lock_src;
			break;
		default:
			break;
		}

		var button_lock1 = $("<img />").attr("src", inStatus);
		var button_lock2 = $("<img />").attr("src", outStatus);
		var button_lock3 = $("<img />").attr("src", alertStatus);
		
		var ba = $("<th scope='row' class='lac_cid'></th>").html(
				bsObj.lac + '-' + bsObj.cid);
		var bb = $("<td class = 'button_in' ></td>").append(button_lock1);
		var bc = $("<td class = 'button_out' ></td>").append(button_lock2);
		var bd = $("<td class = 'button_alert' ></td>").append(button_lock3);

		var newline = $("<tr></tr>")
		newline.append(ba).append(bb).append(bc).append(bd);
		newline.attr("class", "bs_infor_line");

		return newline;
	}

	ns.creatInforLine = creatInforLine;
	ns.creatInforHeader = creatInforHeader;
	ns.clickItem = clickItem;

})()