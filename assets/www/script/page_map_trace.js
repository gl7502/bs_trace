var hetulab;

(function() {
	var internet = hetulab.bstrace.internet;
	var MAPCONTROL = hetulab.bstrace.MAPCONTROL;
	var CELL_MESSAGE_INFOR = hetulab.bstrace.CELL_MESSAGE_INFOR;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;
	
	var position = 0;
	var locations = [];
	var map = null;
	var beijing;
	var zhengzhou;
	var iterator = 0;
	var markers = [];
	var flashBsTimer;
	

	
	//地图初始化
	function initialize() {
		$("#map_progress").hide();
		
		beijing = new google.maps.LatLng(39.906049, 116.407356);
		zhengzhou = new google.maps.LatLng(34.763615,113.623352);
		
		position = 0;
		markers = [];
		iterator = 0;
		
		var mapOptions = {
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
			center : beijing
		};
		map = new google.maps.Map(document.getElementById("map_canvas"),
				mapOptions);
	};
	
	// Start page
	$(document).on("pagecreate", "#map_home", function() {
		// 进行 网络检测，如果没有网络给出 告警。		
		if(internet.hasNetWork()){
//			$.getScript("http://maps.google.com/maps/api/js?sensor=false&callback=initialize");	
			$("#map_progress").html("地图加载中……").show();
			initialize();
		}else{
			internet.showNoNetworkWarning();
		}	

	});
	
	$(document).on("pageshow", "#map_home", function() {
		if(map){
			setMapArea();
			google.maps.event.trigger(map, 'resize');	
			 
			flashBsTimer = setInterval(function() {
					trace();
			}, 1000);
		}

	});
	

	function setMapArea() {
		// 设置显示区域 测试表明，该值仅代表 content 区域的大小
		var height1 = $('#map_home').height();
		var width = $('#map_home').width();

		$('#map_canvas').css({
			'height' : height1 + 'px',
			'width' : width + 'px'
		});
	}
	
	function trace() {
		locations = CELL_MESSAGE_INFOR.getLocations(position);
		
		navigator.geolocation.getCurrentPosition(onGpsSuccess, onGpsError,{enableHighAccuracy: true });			

//		console.log("----trace locations -----" + locations);
		
		if (locations == null)
			return;
//		console.log("----trace locations length-----" + locations.length);
		
		if (locations.length < 1) {
//			console.log("----trace locations.length < 1-----");
			return;
		} else {
			console.log("----trace add marker-----");
			iterator = 0;
			position = position + locations.length;
			addBsMarker();
		}
	}
	
    // onSuccess Geolocation
    function onGpsSuccess(position) {
    	console.log("---map trace postion onSuccess-------" + position.coords.latitude);
    	showPostion(position);
    }

    function showPostion(pos){
    	var posLatLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
		var marker1 = new google.maps.Marker({
			position : posLatLng,
			map : map,
			title : "pos"
		});	
    }
    
    // onError Callback receives a PositionError object
    function onGpsError(error) {
    	console.log("---geolocation onError-------" );
//    	alert("获取位置信息失败！");
//        alert('code: '    + error.code    + '\n' +
//              'message: ' + error.message + '\n');
    }

	function addBsMarker() {
//		console.log("----add bs marker adding-----");
		for ( var i = 0; i < locations.length; i++) {
			setTimeout(function() {
				addMarker();
			}, i * 200);
		}		
	}

	function addMarker() {
	
		console.log("---trace addMarker-----------");
		var newlatlng = new google.maps.LatLng(locations[iterator].latitude,
				locations[iterator].longitude);
		
		map.panTo(newlatlng);
		
		var marker = new google.maps.Marker({
			position : newlatlng,
			map : map,
			icon : "../images/map_icon/basestation.png",
			draggable : false
		// animation: google.maps.Animation.DROP
		});
		
		var contentString = '<div id="content">' + '<div id="siteNotice">'
		+ 'lac: ' + locations[iterator].lac + '  cid: ' + locations[iterator].cid + '</div>'
		+ '<div id="bodyContent">' + '地址: ' + locations[iterator].region + locations[iterator].city
		+ locations[iterator].street + '</div>' + '</div>';

		var infowindow = new google.maps.InfoWindow({
			content : contentString
		});
		
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
		
		if(markers != null && markers.length > 0){
			var oldMarker = markers[markers.length - 1];
			oldMarker.setIcon("../images/map_icon/basestation_old.png");			
		}
		
		markers.push(marker);
		iterator++;
	}

})();