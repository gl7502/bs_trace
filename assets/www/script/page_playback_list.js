var hetulab;

(function() {
	var fileOp = hetulab.bstrace.fileoperate;
	
	$(document).on("pageshow", "#page_playback_list", function() {
		showBsInfor();			
	});
	
	function showBsInfor() {
		console.log("----show bs infor first in second line -----");
		// 每次增加新基站信息
		var bs_list = fileOp.getPlaybackList();
		console.log("----show bs infor bs_list -----" + bs_list);
		if (bs_list == null)
			return;
		if (bs_list.length < 1)
			return;
		var length = bs_list.length;
		console.log("----show bs infor bs_list.length -----" + length);
		
		$("#playback_header").empty();
		$("#playbackHistory").empty();
		
		$("#playback_header").append(creatInforHeader());
		
		for ( var i = 0; i < length; i++) {
			console.log("----show bs infor first in second line -----");
			var showObj = {lac :bs_list[i].lac,
					cid :bs_list[i].cid,
					time : bs_list[i].time};
//			console.log("----show bs infor showObj.time -----" + showObj.time);
			var newline = creatInforLine(showObj);
			$("#playbackHistory").append(newline);
		}
	}
	
	function creatInforHeader() {
		var newline = $("<tr style='background-color: #A42D00'></tr>").append(
				"<th scope='col' style='color: white'>LAC-CID</th>").append(
				"<th scope='col' style='color: white'>时间</th>");
		return newline;
	}

	// 返回一个 $ 变量，表示基站对象的一行
	function creatInforLine(bsObj) {
		
		var cidTemp = bsObj.cid;
		if(cidTemp > 100000){
			cidTemp = cidTemp & 0xffff;
		}
		
		var ba = $("<th scope='row' class='lac_cid'></th>").html(
				bsObj.lac + '-' + cidTemp);
		var date = new Date(bsObj.time);
//		console.log("----show bs infor creatInforLine date-----" + date.toString());
		
		var bb = $("<td class = 'list_view' ></td>").html(
				date.getFullYear()+ ":" + (date.getMonth() + 1) + ":"
				+ date.getDate() + " " + date.getHours() + ":" 
				+ date.getMinutes() + ":" + date.getSeconds());

		var newline = $("<tr></tr>")
		newline.append(ba).append(bb);
		
		return newline;
	}

})();