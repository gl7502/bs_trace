var hetulab;

if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.lock_manager = {};

(function(){
		
	var lock_manager = hetulab.bstrace.lock_manager;
	
	var dirHelper;
	var lock_list = new Array();
	
	function getLockList(){
		return lock_list;
	}

	var getRootName = function(fileSystem) {
//		console.log(fileSystem.name);
//		console.log(fileSystem.root.name);
		// 取得 root
		var rootEntry = fileSystem.root;

		rootEntry.getDirectory("helper", {
			create : true,
			exclusive : false
		}, function(dir) {
//			console.log("zzzzzzzzzzzzz dirHelper1" + dir.name);
			dirHelper = dir;

			// 创建 或 找回 lock 文件
			dirHelper.getFile("lock.txt", {
				create : true,
				exclusive : false
			}, function(tempfile) {
//				console.log("zzzzzzzzzzzzz lock" + tempfile.name);
				tempfile.file(function(file) {
					_getlock_list(file);
				}, fileFail);
			}, fileFail);

		}, fileFail);
	}

	var fileFail = function(error) {
		alert(error.code);
	}
	
	// 从 lock.txt 文件中 读出 数据保存在对象数组中
	function _getlock_list(file) {
		var reader = new FileReader();
		// 锁定基站表
		reader.onloadend = function(evt) {
//			console.log("-----lock file result--------"+reader.result);

			var lockLineArray = reader.result.split("\n");
//			console.log("-----lockLineArray --------" + lockLineArray.length);
			for ( var i=0;i<lockLineArray.length;i++) {
				var lineArray = lockLineArray[i].split(",");
//				console.log("------lineArray-----" + lineArray.length);
				if(lineArray.length < 3) break; //跳过空行

				var lineObj = {}
				lineObj.lac = parseInt(lineArray[0]);
				lineObj.cid = parseInt(lineArray[1]);
				lineObj.lock_status = parseInt(lineArray[2]);
				lineObj.alert = parseInt(lineArray[3]);;  //提醒标志 缺省为打开（1）
				lock_list.push(lineObj);
			}
//			console.log("-----lock file lock_list --------" + lock_list.length);
		};
		reader.readAsText(file);
	}
	
	function updateLockFile() {		
		// 创建 或 找回 lock 文件
		dirHelper.getFile("lock.txt", {
			create : true,
			exclusive : false
		}, function(tempfile) {
//			console.log("zzzzzzzzzzzzz lock" + tempfile.name);
			tempfile.createWriter(function(writer) {
				writer.truncate(0);
//				console.log("----lockfile-- lock_list truncate---");
				writer.onwriteend = function(evt){
					writer.seek(0);
//					console.log("----lockfile- seek---");
					var content ='';
					for ( var i = 0; i < lock_list.length; i++) {
						var lockRecord = lock_list[i].lac + "," + lock_list[i].cid + ","
								+ lock_list[i].lock_status + "," + lock_list[i].alert +"\n";
						content = content + lockRecord;
//						console.log("----lock list string----" + lockRecord);						
					}
					writer.write(content);
					writer.onwriteend = function(evt){
//						console.log("---lock file writer--" + content);
					}					
				}
			}, fileFail);
		}, fileFail);
	}

	document.addEventListener('deviceready', onDeviceReady, false);
	// request the persistent file system
	function onDeviceReady() {
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, getRootName,
				fileFail);
	}
	
	// 0 未锁; 1 已锁
	function getInLockStatus(lac, cid) {
		var result = find(lac, cid);
		return _getInLockStatus(result);
	}

	function _getInLockStatus(result){
		if (result == 0)
			return 0;
		if (result.lock_status == 2) {
			return 0;
		} else if (result.lock_status == 1 || result.lock_status == 3) {
			return 1;
		}
	}
	
	// 0 未锁 1 已锁
	function getOutLockStatus(lac, cid) {
		var result = find(lac, cid);
		return _getOutLockStatus(result);
	}
	
	function _getOutLockStatus(result){
		if (result == 0)
			return 0;
		if (result.lock_status == 1) {
			return 0;
		} else if (result.lock_status == 2 || result.lock_status == 3) {
			return 1;
		}
	}

	// 0 no_lock 1 in_lock 2 out_lock 3 double_lock
	function getLockStatus(lac, cid) {
		var result = find(lac, cid);
		if (result == 0) {
			return 0;
		} else {
			return result.lock_status;
		}
	}
	
	function setLockStatus(lac,cid,in_status,out_status){
		//表明已经有 此项 ,则 改变成新的
		if(getInLockStatus(lac,cid) || getOutLockStatus(lac, cid)){
			if(getInLockStatus(lac,cid) != in_status){
				changeInLockStatus(lac, cid);
			}
			if(getOutLockStatus(lac,cid) != out_status){
				changeOutLockStatus(lac,cid);
			}
		}else{
			if(in_status == 1){
				changeInLockStatus(lac, cid);
			}
			if(out_status == 1){
				changeOutLockStatus(lac,cid);
			}			
		}		
	}
	
	function changeInLockStatus(lac, cid) {
		var ret;
		var findRet = 0,findLoc = -1;
		
		for ( var i = 0; i < lock_list.length; i++) {
			if (lock_list[i].lac == lac && lock_list[i].cid == cid){
				findRet = lock_list[i];
				findLoc = i;
				break;
			}
		}
		
//		console.log("---changeInLockStatus---findRet---" + findLoc +"-" + findRet );
		
		var oldIn = _getInLockStatus(findRet);
		var oldOut = _getOutLockStatus(findRet);
		
		// 原来没有记录，需要追加一行
		if (oldIn == 0 && oldOut == 0) {
			var obj = {}
			obj.lac = lac;
			obj.cid = cid;
			obj.lock_status = 1;
			//新增锁定基站时，提醒 缺省打开。
			obj.alert = 1;
			lock_list.push(obj);
			ret = 1;
			// console.log("---chang in lockstatus--add line--");
		}
		// 如果原来仅有 InLock ，就要取消此项
		else if (oldIn == 1 && oldOut == 0) {
			lock_list.splice(findLoc, 1);
//			 console.log("---chang in lockstatus--delete line--" + findLoc);
			ret = 0;
		}
		// 如果原来仅有 OutLock ，就要 改变相应的值
		else if (oldIn == 0 && oldOut == 1) {
			findRet.lock_status = 3;
			ret = 1;
		}
		// 如果原来都Lock ，就要 改变相应的值
		else if (oldIn == 1 && oldOut == 1) {
			findRet.lock_status = 2;
			ret = 0;
		}
		updateLockFile();
		return ret;
}
	
	// 改变 outStatus 状态
	function changeOutLockStatus(lac, cid) {
		var ret;
		var findRet = 0,findLoc = -1;
		
		for ( var i = 0; i < lock_list.length; i++) {
			if (lock_list[i].lac == lac && lock_list[i].cid == cid){
				findRet = lock_list[i];
				findLoc = i;
				break;
			}
		}
		
		var oldIn = _getInLockStatus(findRet);
		var oldOut = _getOutLockStatus(findRet);
		// console.log("----old status---" + oldIn + "," + oldOut);
		// 原来没有记录，需要追加一行
		if (oldIn == 0 && oldOut == 0) {
			var obj = {}
			obj.lac = lac;
			obj.cid = cid;
			obj.lock_status = 2;
			//新增锁定基站时，声音和震动提醒 缺省打开。
			obj.alert = 1;
			lock_list.push(obj);
			ret = 1
			// console.log("----changeOutLockStatus add----" + lac + cid);
		}
		// 如果原来仅有 outLock ，就要取消此项
		else if (oldIn == 0 && oldOut == 1) {
			lock_list.splice(findLoc, 1);
//			 console.log("---chang out lockstatus--delete line--" + findLoc);
			ret = 0;
		}
		// 如果原来仅有 inLock ，就要 改变相应的值
		else if (oldIn == 1 && oldOut == 0) {
			findRet.lock_status = 3;
			ret = 1;
		}
		// 如果原来都Lock ，就要 改变相应的值
		else if (oldIn == 1 && oldOut == 1) {
			findRet.lock_status = 1;
			ret = 0;
		}
		updateLockFile();
		return ret;
	}

	function find(lac, cid) {
		for ( var i = 0; i < lock_list.length; i++) {
			if (lock_list[i].lac == lac && lock_list[i].cid == cid)
				return lock_list[i];
		}
		return 0;
	}
	
	function getAlertStatus(lac, cid){
		var ret = find(lac,cid);
		if(ret != 0){
			return ret.alert;
		}
		return -1;
	}
	
	function changeAlertStatus(lac, cid){
		var result = find(lac, cid);
		//这种情况应该不可能
		if(result == 0) alert("该基站没有锁定设置，不能改变声音告警状态!");
		if(result.alert == 1){
			result.alert = 0;
		}else {
			result.alert = 1;
		}
		updateLockFile();
		return result.alert;
	}
	
	lock_manager.updateLockFile = updateLockFile;
	lock_manager.getLockList = getLockList;
	lock_manager.getInLockStatus = getInLockStatus;
	lock_manager.getOutLockStatus = getOutLockStatus;
	lock_manager.getLockStatus = getLockStatus;
	lock_manager.changeInLockStatus = changeInLockStatus;
	lock_manager.changeOutLockStatus = changeOutLockStatus;
	lock_manager.changeAlertStatus = changeAlertStatus;
	lock_manager.getAlertStatus = getAlertStatus;
	lock_manager.setLockStatus = setLockStatus;
	
})()