var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.DATA_ACCESSOR = {};

(function(){
	var internet = hetulab.bstrace.internet;
	var db = hetulab.bstrace.DATABASE;
	
	var cb ; 
	
	function getLocation(lac,cid , callback){
		cb = callback;

		db.getBsLocation(lac,cid,success,fail);
	}

	function success(lac,cid,loc){
		cb(lac,cid,loc);
	}
	
	function fail(lac,cid,error){
//		console.log("---accessor fail---" + error);
//		console.log("---accessor fail lac---" + lac);
		internet.requestLocation(lac,cid,requestCallback);
	}
	
	function requestCallback(lac,cid,data){

		if(!!data["location"]){
			
			db.insertBsLocation(lac,cid,data);
			cb(lac,cid,data);
			
//			console.log("--accessor--" + JSON.stringify(data));
				
		}else{
			cb(0,0,null);
		}		
	}
	
	var ns = hetulab.bstrace.DATA_ACCESSOR;
	ns.getLocation = getLocation;
	
})()