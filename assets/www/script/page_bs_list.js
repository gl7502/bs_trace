var hetulab;

(function() {

	var fileOp = hetulab.bstrace.fileoperate;
	var lock_manage = hetulab.bstrace.lock_manager;
	var mapcontrol = hetulab.bstrace.MAPCONTROL;
	var internet = hetulab.bstrace.internet;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;
	var infor_show = hetulab.bstrace.infor_show;

	var CELL_MESSAGE_INFOR = hetulab.bstrace.CELL_MESSAGE_INFOR;

	var operatorName;
	var location = null;

	var line_his = 0;
	var bs_list_position = -1;

	// 用于记录选中状态的数组；其下标与 bs_list对应
	var checkStatus = [];

	var flashBsTimer;

	var icon_check = "../images/icons/check50-60.png";
	var icon_disable = "../images/icons/disable50-60.jpg";

	document.addEventListener('deviceready', function() {

		$("#bsHis_heade").hide();
		// bs_new = getBsInfor();
		// 注册锁定按钮点击事件
		$("#bsHistory").on("click", ".check_map", function(evt) {
			clickItem(this);
		});

		// $(".start_gather").click(function(evt) {
		// evt.preventDefault();
		// console.log("----开始采集-----" + fileOp.has_started());
		// if(fileOp.has_started() == true){
		// console.log("----正在采集中-----");
		// alert("正在采集中……");
		// }else{
		// console.log("----创建采集文件-----");
		// fileOp.createSiteFile();
		// }
		// return false;
		// });

		// $(".stop_gather").click(function(evt){
		// evt.preventDefault();
		// if(fileOp.has_started()){
		// return true;
		// }else{
		// var time = new Date();
		// var filename = "黑猫" + time.getFullYear() + "年" + (time.getMonth() +
		// 1) + "月"
		// +time.getDate() + "日" + time.getHours() + time.getMinutes()
		// + time.getSeconds() + time.getMilliseconds();
		// console.log("page_file_name_dialoge-filename---" + filename);
		// fileOp.renameSiteFile(filename);
		// return false;
		// }
		// });

	}, true);

	function clickItem(tab) {
		var line_num = $(tab).siblings(".check_num").html()
		console.log("page_bs_list-clickItem---" + line_num);

		if (checkStatus[line_num]) {
			$(tab).children().attr('src', icon_disable);
		} else {
			$(tab).children().attr('src', icon_check);
		}

		checkStatus[line_num] = !checkStatus[line_num];

	}
	; // function(evt) {

	// 当应用处于 后台 时需要做的处理
	var onPause = function() {
		lock_manage.updateLockFile();
	}

	$(document).on("pagecreate", "#page_bs_list", function() {

		// CELL_MESSAGE_INFOR.initCollect();
		// flashBsTimer = setInterval(function() {
		// CELL_MESSAGE_INFOR.getCellInfor();
		// // showOperatorName();
		// // showSignalStrength();
		// showBsInfor();
		// }, 1000);
	});

	$(document).on("pageshow", "#page_bs_list", function() {
		console.log("----开始采集-----" + fileOp.has_started());

		document.addEventListener("backbutton", onBackKeyDown, false);
		
		if (fileOp.has_started() == true) {
			console.log("----正在采集中-----");
			// alert("正在采集中……");
		} else {
			console.log("----创建采集文件-----");
			fileOp.createSiteFile();
		}

		CELL_MESSAGE_INFOR.initCollect();
		flashBsTimer = setInterval(function() {

			CELL_MESSAGE_INFOR.getCellInfor();
			// showOperatorName();
			// showSignalStrength();
			showBsInfor();
		}, 1000);

	});

	function onBackKeyDown() {
		// alert("检测到返回键");
		showConfirm();
	}

	$(document).on("pagebeforehide", "#page_bs_list", function(evt, ui) {
		document.removeEventListener("backbutton", onBackKeyDown, false);
		bs_list = [];
	});

	// callback function
	function onConfirm(button) {
		// if press 'Yes'
		if (button === 1) {
			// document.removeEventListener("backbutton", function(){alert("移除
			// 事件");}, false);
			$.mobile.changePage($("#page_main"), {
				transition : "slide"
			});
			return false;
		} else {
			return false;
		}
	}

	// PhoneGap Notification 提供的 Confirm API
	function showConfirm() {
		navigator.notification.confirm(
				'你将返回首页。这将导致本次采集终止，并且采集数据不会被保存。你确定要返回首页吗?', // message
				onConfirm, // callback function
				'终止采集确认', // title
				'是的,取消' // confirm 選項，用逗號隔開
		);
	}

	function showOperatorName() {
		$("#operator").html(
				"运营商：" + CELL_MESSAGE_INFOR.getOperatorName() + "  "
						+ CELL_MESSAGE_INFOR.getNetType());
	}

	function showSignalStrength() {
		$("#bs_infor").empty();
		$("#bs_infor").html("当前基站信号强度：" + CELL_MESSAGE_INFOR.getSigStren());
	}

	function creatInforHeader() {
		var newline = $("<tr style='background-color: #A42D00'></tr>").append(
				"<th scope='col' style='color: white'>大区</th>").append(
				"<th scope='col' style='color: white'>小区</th>").append(
				"<th scope='col' style='color: white'>时间</th>").append(
				"<th scope='col' style='color: white'>信号强度</th>").append(
				"<th scope='col' style='color: white'>地图</th>");
		return newline;
	}

	function creatCdmaHeader() {
		var newline = $("<tr style='background-color: #A42D00'></tr>").append(
				"<th scope='col' style='color: white'>系统</th>").append(
				"<th scope='col' style='color: white'>网络</th>").append(
				"<th scope='col' style='color: white'>基站</th>").append(
				"<th scope='col' style='color: white'>时间</th>").append(
				"<th scope='col' style='color: white'>信号强度</th>").append(
				"<th scope='col' style='color: white'>地图</th>");
		return newline;
	}

	function getTimeShowString(date) {
		return date.getFullYear() + ":" + (date.getMonth() + 1) + ":"
				+ date.getDate() + " " + date.getHours() + ":"
				+ date.getMinutes() + ":" + date.getSeconds();
	}

	function creatInforLine(bs) {

		var a1 = $("<td ></td>").append(bs.lac);
		var a2 = $("<td ></td>").append((bs.cid & 0xffff));
		var a3 = $("<td ></td>").append(getTimeShowString(bs.time));
		var a4 = $("<td ></td>").append(bs.rssi);

		var newline = $("<tr></tr>").append(a1).append(a2).append(a3)
				.append(a4);

		return newline;
	}

	function creatCdmaLine(bs) {

		var a1 = $("<td ></td>").append(bs.sid);
		var a2 = $("<td ></td>").append((bs.nid));
		var a3 = $("<td ></td>").append((bs.cid));
		var a4 = $("<td ></td>").append(getTimeShowString(bs.time));
		var a5 = $("<td ></td>").append(bs.rssi);

		var newline = $("<tr></tr>").append(a1).append(a2).append(a3)
				.append(a4).append(a5);

		return newline;
	}

	function checkMapIcon(lineNum) {
		var map_check;
		if (checkStatus[lineNum] != true) {
			map_check = $("<img />").attr("src", icon_disable);
		} else {
			map_check = $("<img />").attr("src", icon_check);
		}

		var checkTd = $("<td class = 'check_map'></td>").append(map_check);

		return checkTd;
	}

	function checkMapNum(lineNum) {
		return $("<td class = 'check_num' style='display:none;'></td>").append(
				lineNum);
	}

	function showBsInfor() {
		// 每次增加新基站信息
		var bs_list = CELL_MESSAGE_INFOR.getBsList();
		if (bs_list == null)
			return;
		if (bs_list.length < 1)
			return;
		var length = bs_list.length;

		$("#operator").html(
				"运营商：" + bs_list[length - 1].operatorname + "  "
						+ getNetTypeStr(parseInt(bs_list[length - 1].net)));

		$("#bsHis_header").empty();
		$("#bsHistory").empty();

		var handType = CELL_MESSAGE_INFOR.getPhoneType();
		// console.log("page_bs_list.js--showBsInfor()--handType--" + handType);

		var newline;
		if (handType == 'GSM') {
			$("#bsHis_header").append(creatInforHeader());
			for ( var i = 0; i < length; i++) {
				newline = creatInforLine(bs_list[i]).append(checkMapIcon(i))
						.append(checkMapNum(i));
				$("#bsHistory").prepend(newline);
			}
		} else if (handType == 'CDMA') {
			$("#bsHis_header").append(creatCdmaHeader());
			for ( var i = 0; i < length; i++) {
				newline = creatCdmaLine(bs_list[i]).append(checkMapIcon(i))
						.append(checkMapNum(i));
				$("#bsHistory").prepend(newline);
			}
		}

		// var newline;
		// for ( var i = 0; i < length; i++) {
		// if(bs_list[i].handset == 'GSM'){
		// newline =
		// creatInforLine(bs_list[i]).append(checkMapIcon(i)).append(checkMapNum(i));
		// }else if(bs_list[i].handset == 'CDMA'){
		// newline =
		// creatCdmaLine(bs_list[i]).append(checkMapIcon(i)).append(checkMapNum(i));
		// }
		//			
		// $("#bsHistory").prepend(newline);
		//
		// }
		newline.css("background-color", "#99FF99");

		// for test
		// $("#bs_infor").empty();
		// $("#bs_infor").html("当前基站列表长度：" + length);
	}

	function getNetTypeStr(type) {
		var netType;
		switch (type) {
		case 0:
			netType = "NETWORK_TYPE_UNKNOWN";
			break;
		case 1:
			netType = "NETWORK_TYPE_GPRS";
			break;
		case 2:
			netType = "NETWORK_TYPE_EDGE";
			break;
		case 3:
			netType = "NETWORK_TYPE_UMTS";
			break;
		case 8:
			netType = "NETWORK_TYPE_HSDPA";
			break;
		case 9:
			netType = "NETWORK_TYPE_HSUPA";
			break;
		case 10:
			netType = "NETWORK_TYPE_HSPA";
			break;
		case 4:
			netType = "NETWORK_TYPE_CDMA";
			break;
		case 5:
			netType = "NETWORK_TYPE_EVDO_0";
			break;
		case 6:
			netType = "NETWORK_TYPE_EVDO_A";
			break;
		case 12:
			netType = "NETWORK_TYPE_EVDO_B";
			break;
		case 7:
			netType = "NETWORK_TYPE_1xRTT";
			break;
		case 11:
			netType = "NETWORK_TYPE_IDEN";
			break;
		case 13:
			netType = "NETWORK_TYPE_LTE";
			break;
		case 14:
			netType = "NETWORK_TYPE_EHRPD";
			break;
		case 15:
			netType = "NETWORK_TYPE_HSPAP";
			break;
		default:
			netType = "ERR";
			break;
		}
		return netType;
	}

})()
