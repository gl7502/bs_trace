var hetulab;

(function() {
	var fileOp = hetulab.bstrace.fileoperate;
	var internet = hetulab.bstrace.internet;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;
	
	var markers = [];
	var beijing;
	
	function initialize() {
		beijing = new google.maps.LatLng(39.906049, 116.407356);
		
		position = 0;
		markers = [];
		iterator = 0;
		
		var mapOptions = {
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
			center : beijing
		};
		map = new google.maps.Map(document.getElementById("map_playback_canvas"),
				mapOptions);
	}
	
	$(document).on("pageshow", "#page_playback_map", function() {
		// 进行 网络检测，如果没有网络给出 告警。
		internet.showNoNetworkWarning();		
		
		setMapArea();
		
		initialize();
		
		google.maps.event.trigger(map, 'resize');
		 
		 showPlayback();
	});
	
	function setMapArea() {
		// 设置显示区域 测试表明，该值仅代表 content 区域的大小
		var height1 = $('#page_playback_map').height();
		var width = $('#page_playback_map').width();

		$('#map_playback_canvas').css({
			'height' : height1 + 'px',
			'width' : width + 'px'
		});
	}
	
	function showPlayback(){
		console.log("----show bs infor first in second line -----");
		// 每次增加新基站信息
		var bs_list = fileOp.getPlaybackList();
		console.log("----show bs infor bs_list.length -----" + bs_list.length);	
		
		for ( var i = 0; i < bs_list.length; i++) {
//			console.log("----updateLocationsn-bs_list[0]----" + bs_list[0].lac);
			console.log("----showPlayback-bs_list[i].lac----" + bs_list[i].lac);
			setTimeout(askLocation(bs_list[i].lac, bs_list[i].cid), i * 200);	
		}
	}
		
	function askLocation(lac,cid) {
		accessor.getLocation(lac,cid, addMarker);
      }
	
	function addMarker(lac,cid,loc) {
		
//		console.log("---trace addMarker-----------" + lac);
		var newlatlng = new google.maps.LatLng(loc.location.latitude,
				loc.location.longitude);
		
		map.panTo(newlatlng);
		
		var marker = new google.maps.Marker({
			position : newlatlng,
			map : map,
			icon : "../images/map_icon/basestation_old.png",
			draggable : false
		// animation: google.maps.Animation.DROP
		});
		
		var contentString = '<div id="content">' + '<div id="siteNotice">'
		+ 'lac: ' + lac + '  cid: ' + cid + '</div>'
		+ '<div id="bodyContent">' + '地址: ' + loc.location.address.region + loc.location.address.city
		+ loc.location.address.street + '</div>' + '</div>';

		var infowindow = new google.maps.InfoWindow({
			content : contentString
		});
		
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
		
//		if(markers != null && markers.length > 0){
//			var oldMarker = markers[markers.length - 1];
//			oldMarker.setIcon("../images/icons/basestation_old.png");			
//		}
		
		markers.push(marker);
		iterator++;
	}
	
})();