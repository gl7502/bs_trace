var hetulab;

(function() {
	var lock_manage = hetulab.bstrace.lock_manager;
	var lock_list = lock_manage.getLockList();
	var infor_show = hetulab.bstrace.infor_show;

	var obj1 = {}
	var inLock = 0,outLock = 0,alarm = 0;

	$(document).on("click", "#lock_manage_ok", function(event) {
		event.preventDefault();

		// console.log("------updateLockList------被点击")
		if (!!$("#input_scale").attr("value")) {
			var scale16 = ($("#input_scale").attr("value") == 'on');
		} else {
			scale16 = false;
		}

		var mustInput = true;

		if (!!$("#basic").attr("value")) {
			var lac = $("#basic").attr("value");
		} else {
			mustInput = false;
		}

		if (!!$("#basic1").attr("value")) {
			var cid = $("#basic1").attr("value");
		} else {
			mustInput = false;
		}

		if (mustInput == false) {
			alert("LAC 或者 CID 不能为空！");
			return false;
		}
		
		if (scale16) {
			obj1.lac = parseInt(lac, 16);
			obj1.cid = parseInt(cid, 16);
		} else {
			obj1.lac = parseInt(lac, 10);
			obj1.cid = parseInt(cid, 10);
			
			console.log("------输入值转换-----" + obj1.lac);
			
			if(isNaN(obj1.lac) || isNaN(obj1.cid)){
				alert("输入了非法数据");
				return false;
			}
		}

		console.log("------lockStatus-----" + "teststest");

		if (!!$("#lock_in").attr("value")) {
			if ($("#lock_in").attr("value") == 'on'){
					inLock = 1;
				};
		} else {
			inLock = 0;
		}

		if (!!$("#lock_out").attr("value")) {
			if($("#lock_out").attr("value") == 'on'){
				outLock = 1
			};
		} else {
			outLock = 0;
		}

		if(inLock == 0 && outLock == 0){
			alert("进入锁定 和 离开锁定 至少要选择一个！");
			return false;
		}else{
			lock_manage.setLockStatus(lac,cid,inLock,outLock);
		}				
		
		if (!!$("#lock_alarm").attr("value")) {
			if($("#lock_alarm").attr("value") == 'on'){
				alarm = 1;
			}			
		} else {
			alarm = 0;
		}
		
		var alarm_old = lock_manage.getAlertStatus(lac,cid);
		if(alarm_old == -1){
			alert("该基站没有设置锁定，所以不能设置告警。")
			return false;
		}else if(alarm_old != alarm){
			lock_manage.changeAlertStatus(lac,cid);
		}

//		// obj1.lock_status = lockStatus;
//		obj1.lock_status = lock_manage.getLockStatus(obj1.lac, obj1.cid);
//		// console.log("------lockStatus-----" + obj1.lock_status);
//		obj1.alert = lock_manage.getAlertStatus(obj1.lac, obj1.cid);
//
//		// lock_list.push(obj1);
//		$("#lock_add_table_label").empty();
//		$("#lock_add_tbl_head").empty();
//		$("#lock_add_tbl").empty();
//
//		$("#lock_add_table_label").text("请在下表中设置该基站的锁定及告警状态")
//		$("#lock_add_table").show();
//		$("#lock_add_tbl_head").append(infor_show.creatInforHeader());
//
//		$("#lock_add_tbl").prepend(infor_show.creatInforLine(obj1));

//		return false;
		return true;
	});

	$(document).on("pagecreate", "#page_lock_manage_add", function() {
		$("#lock_add_table").on("click", "td", function(evt) {
			infor_show.clickItem(this);

		}); // function(evt) {

		$("#lock_manage_go_on").click(function(evt) {
			$("#basic1").attr("value", "");
			$("#lock_add_table_label").empty();
			$("#lock_add_tbl_head").empty();
			$("#lock_add_tbl").empty();

		});
	});

	$(document).on("pagehide", "#page_lock_manage_add", function() {
		$("#basic").attr("value", "");
		$("#basic1").attr("value", "");
		$("#lock_add_table_label").empty();
		$("#lock_add_tbl_head").empty();
		$("#lock_add_tbl").empty();
	});

})()
