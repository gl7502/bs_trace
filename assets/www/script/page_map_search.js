var hetulab;

(function() {
	var internet = hetulab.bstrace.internet;
	var MAPCONTROL = hetulab.bstrace.MAPCONTROL;
	var CELL_MESSAGE_INFOR = hetulab.bstrace.CELL_MESSAGE_INFOR;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;

	// Start page
	$(document).on("pageshow", "#map_search", function() {
		// 进行 网络检测，如果没有网络给出 告警。
		internet.showNoNetworkWarning();

		setMapArea();
		
		addBsMarker();

	});

	function setMapArea() {
		// 设置显示区域 测试表明，该值仅代表 content 区域的大小
		var height1 = $('#map_search').height();
		var width = $('#map_search').width();

		$('#map_canvas_search').css({
			'height' : height1 + 'px',
			'width' : width + 'px'
		});
	}

	function addBsMarker() {
		var locations = MAPCONTROL.getCurLoc();
		
		var myLatlng = new google.maps.LatLng(locations.latitude, locations.longitude);

		var myOptions = {
			center : myLatlng,
			zoom : 16,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		
		var map = new google.maps.Map(document.getElementById("map_canvas_search"),
				myOptions);

		var marker = new google.maps.Marker({
			position : myLatlng,
			map : map,
			icon:"../images/map_icon/basestation.png",
			title : "baseStation!"
		});

		var contentString = '<div id="content">' + '<div id="siteNotice">'
				+ 'lac: ' + locations.lac + '  cid: ' + locations.cid + '</div>'
				+ '<div id="bodyContent">' + '地址: ' + locations.region + locations.city
				+ locations.street + '</div>' + '</div>';

		var infowindow = new google.maps.InfoWindow({
			content : contentString
		});

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
		
		var pos = MAPCONTROL.getPosition();
		console.log("-----pos---",pos.coords.latitude);
		var posLatLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
		
		var marker1 = new google.maps.Marker({
			position : posLatLng,
			map : map,
			title : "curPos"
		});		
	}	

})();
