var hetulab;

(function() {
	var lock_manage = hetulab.bstrace.lock_manager;
	var lock_list = lock_manage.getLockList();
	var internet = hetulab.bstrace.internet;
	var data_accessor = hetulab.bstrace.DATA_ACCESSOR;
	var infor_show = hetulab.bstrace.infor_show;
	var mapcontrol = hetulab.bstrace.MAPCONTROL;

	console.log("---search location mapcontrol-1-" + mapcontrol);

	var search_input = new Array();

	var bsLoc = {};
	var bsObj = {};
	var location = null;

	$(document)
			.on(
					"click",
					"#search_go",
					function(event) {
						event.preventDefault();
						var lac1, cid1;
						var scale16 = false;
						var mustInput1 = true;

						if (!!$("#search_in_v1").attr("value")) {
							lac1 = $("#search_in_v1").attr("value");
						} else {
							mustInput1 = false;
						}

						if (!!$("#search_in_v2").attr("value")) {
							cid1 = $("#search_in_v2").attr("value");
						} else {
							mustInput1 = false;
						}
						
						if (mustInput1 == false) {
							alert("请输入一组数据！")
							return false;
						}

						if (!!$("#checkbox-search").attr("value")) {
							if ($("#checkbox-search").attr("value") == 'on') {
								scale16 = true;
							}
						}

						if (scale16) {
							if (mustInput1) {
								bsObj.lac = parseInt(lac1, 16);
								bsObj.cid = parseInt(cid1, 16);
								search_input.push(obj1);
							}
						} else {
							if (mustInput1) {
								bsObj.lac = parseInt(lac1, 10);
								bsObj.cid = parseInt(cid1, 10);
							}
						}
						
//						console.log("----search location input 1----"+ bsObj.lac + "  " + bsObj.cid);

						bsObj.lock_status = lock_manage.getLockStatus(
								bsObj.lac, bsObj.cid);
//						console.log("------lockStatus-----"	+ bsObj.lock_status);
						bsObj.alert = lock_manage.getAlertStatus(bsObj.lac,
								bsObj.cid);

						$("#lock_search_table_label").empty();
						$("#lock_search_tbl_head").empty();
						$("#lock_search_tbl").empty();

						$("#lock_search_table_label")
								.text("请在下表中设置该基站的锁定及告警状态")
						$("#lock_search_table").show();
						$("#lock_search_tbl_head").append(
								infor_show.creatInforHeader());

						$("#lock_search_tbl").prepend(
								infor_show.creatInforLine(bsObj));

//						console.log("----search location input ----" + bsObj.lac + "  " + bsObj.cid);
						data_accessor.getLocation(bsObj.lac, bsObj.cid,
								accessCallback);
						navigator.geolocation.getCurrentPosition(onGpsSuccess, onGpsError,{enableHighAccuracy: true });
						// sendLocRequest(bsObj);
						return false;
					});

	function accessCallback(lac, cid, data) {
		console.log("--search location--" + lac + ' ' + cid);
		if (lac == 0) {
			$('#address').html("查询不到结果，请检查输入数据");
		} else {
			location = data;
			mapcontrol.setCurLoc(bsObj.lac, bsObj.cid, location);

			var line1 = "<p>LAC:" + lac + " CID:" + cid + " 精度："
					+ data.location.accuracy + "米 </p>";
			var line2 = "<p>地址：" + data.location.address.region
					+ data.location.address.city + data.location.address.street
					+ "</p>";
			$("#address").html(line1 + line2);

			// console.log("--search location--" + location);
			// console.log("--search data--" + data);
			//
			// console.log("--search data--" + data["location"]);
			// console.log("--search location--" + location["location"]);
			//			
			// console.log("--search location--" + JSON.stringify(data));
		}
	}
	
    // onSuccess Geolocation
    function onGpsSuccess(position) {
    	console.log("---geolocation onSuccess-------" + position.coords.latitude);
    	mapcontrol.setPosition(position);  	

    }

    // onError Callback receives a PositionError object
    function onGpsError(error) {
    	console.log("---geolocation onError-------" );
    	alert("获取位置信息失败！");
//        alert('code: '    + error.code    + '\n' +
//              'message: ' + error.message + '\n');
    }

	$(document).on("pageshow", "#page_search_location_input", function() {
		// 进行 网络检测，如果没有网络给出 告警。
		internet.showNoNetworkWarning();
	});

	$(document).on("click", "#search_to_map", function(evt) {
		evt.preventDefault();
		console.log("---search location mapcontrol-2-" + mapcontrol);
		if (location) {
			return true;
		} else {
			alert("当有没有有效的位置数据！")
			return false;
		}
	});

	$(document).on("pagecreate", "#page_search_location_input", function() {
		$("#lock_search_table").on("click", "td", function(evt) {
			infor_show.clickItem(this);

		}); // function(evt) {

		$("#search_reset").click(function(evt) {
			$("#search_in_v2").attr("value", "");
			$("#lock_search_table_label").empty();
			$("#lock_search_tbl_head").empty();
			$("#lock_search_tbl").empty();
			$("#address").empty();
		});
	});
	
	$(document).on("pagehide", "#page_search_location_input", function() {

			$("#search_in_v2").attr("value", "");
			$("#lock_search_table_label").empty();
			$("#lock_search_tbl_head").empty();
			$("#lock_search_tbl").empty();
			$("#address").empty();
	});

})()
