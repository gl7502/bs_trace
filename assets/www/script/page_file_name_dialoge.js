var hetulab;

(function() {
	var fileOp = hetulab.bstrace.fileoperate;
	document.addEventListener('deviceready', function() {
		
		$("#confirm_file_save").click(function(evt) {
			evt.preventDefault();
			var filename = $("#fileNameInput").attr("value");
			console.log("page_file_name_dialoge-filename---" + filename);
			if(filename != ''){
				console.log("page_file_name_dialoge-not null filename---" + filename);
				fileOp.renameSiteFile(filename);
			}else{
				var time = new Date();
				filename = time.getFullYear() + "年" + (time.getMonth() + 1) + "月"
					+time.getDate() + "日" + time.getHours() + "时" + time.getMinutes() + "分"
					+ time.getSeconds() + "秒" + time.getMilliseconds();
				console.log("page_file_name_dialoge-filename---" + filename);
				fileOp.renameSiteFile(filename);
			}			
		});		
	
	}, true);
	
})()