var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.fileoperate = {};

(function() {

	document.addEventListener('deviceready', onDeviceReady, false);
	// request the persistent file system
	function onDeviceReady() {
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, getRootName,
				fileFail);
	}

	var fileOp = hetulab.bstrace.fileoperate;
	// 创建 或 取得 可能用到的 文件 和 目录操作句柄
	// 都是相关 的 entry 变量
	var siteFileName;
	var siteFile, siteFileWriter, dirHelper, dirSite, dirAlert;
	var siteBakFile, siteBakFileWriter;
	var dirSiteReader;
	var start_gather = false;

	var playback_list = [];
	var fileList = [];
	var fileNameList = [];

	var showFile;

	function getRootName(fileSystem) {
		// console.log(fileSystem.name);
		// console.log(fileSystem.root.name);
		// 取得 root
		var rootEntry = fileSystem.root;

		rootEntry.getDirectory("helper", {
			create : true,
			exclusive : false
		}, function(dir) {
			console.log("zzzzzzzzzzzzz dirHelper1" + dir.name);
			dirHelper = dir;
			fileOp.dirHelper = dirHelper;

			dirHelper.getDirectory("site", {
				create : true,
				exclusive : false
			}, function(dir) {
				console.log("zzzzzzzzzzzzz dirSite " + dir.name);
				dirSite = dir;
				fileOp.dirSite = dirSite;

				var siteName = "sitebak" + ".tmp";
				dirSite.getFile(siteName, {
					create : true,
					exclusive : false
				}, function(file) {
					// console.log("zzzzzzzzzzzzz siteName" + file.name);
					siteBakFile = file;
					file.createWriter(function(writer) {
						// console.log("zzzzzzzzzzzzz siteFileWriter" + writer);
						// 将文件指针调整到起始处
						writer.truncate(0);
						siteBakFileWriter = writer;
					}, fileFail);
				}, fileFail);

			}, fileFail);

			dirHelper.getDirectory("alert", {
				create : true,
				exclusive : false
			}, function(dir) {
				console.log("zzzzzzzzzzzzz dirAlert" + dir.name);
				dirAlert = dir;
				fileOp.dirAlert = dirAlert;
			}, fileFail);
		}, fileFail);
	}

	function getHelperDir(fileSystem) {
		// console.log(fileSystem.name);
		// console.log(fileSystem.root.name);
		// 取得 root
		var rootEntry = fileSystem.root;
		rootEntry.getDirectory("helper", {
			create : true,
			exclusive : false
		}, function(dir) {
			// console.log("zzzzzzzzzzzzz dirHelper1" + dir.name);
			dirHelper = dir;
			fileOp.dirHelper = dirHelper;
		}, fileFail);
	}

	function delayExecute(check, proc, chkInterval) {
		// default interval = 500ms
		var x = chkInterval || 500;
		var hnd = window.setInterval(function() {
			// if check() return true,
			// stop timer and execute proc()
			if (check()) {
				window.clearInterval(hnd);
				proc();
			}
		}, x);
	}

	function getSiteDir() {
		delayExecute(function() {
			return !!dirHelper;
		}, function() {
			_getSiteDir();
		},500);
	}

	function _getSiteDir() {
		dirHelper.getDirectory("site", {
			create : true,
			exclusive : false
		}, function(dir) {
			console.log("zzzzzzzzzzzzz getSiteDir 1----" + dir.name);
			dirSite = dir;
			console.log("zzzzzzzzzzzzz getSiteDir 2----" + dirSite.name);
			fileOp.dirSite = dirSite;
		}, fileFail);
	}

	function getAlertDir() {
		dirHelper.getDirectory("alert", {
			create : true,
			exclusive : false
		}, function(dir) {
			// console.log("zzzzzzzzzzzzz dirAlert" + dir.name);
			dirAlert = dir;
			fileOp.dirAlert = dirAlert;
		}, fileFail);
	}

	function getSiteFile() {
		// var now = new Date();
		// var siteName = "site" + now.getTime() + ".txt";
		// 为了避免文件过多，每次生成同样的临时文件
		var siteName = "site" + ".tmp";
		dirSite.getFile(siteName, {
			create : true,
			exclusive : false
		}, function(file) {
			// console.log("zzzzzzzzzzzzz siteName" + file.name);
			siteFile = file;
			file.createWriter(function(writer) {
				// console.log("zzzzzzzzzzzzz siteFileWriter" + writer);
				// 将文件指针调整到起始处
				writer.truncate(0);
				siteFileWriter = writer;
			}, fileFail);
		}, fileFail);
	}

	function getBakSiteFile() {
		// var now = new Date();
		// var siteName = "site" + now.getTime() + ".txt";
		// 为了避免文件过多，每次生成同样的临时文件
		var siteName = "sitebak" + ".tmp";
		dirSite.getFile(siteName, {
			create : true,
			exclusive : false
		}, function(file) {
			// console.log("zzzzzzzzzzzzz siteName" + file.name);
			siteFile = file;
			file.createWriter(function(writer) {
				// console.log("zzzzzzzzzzzzz siteFileWriter" + writer);
				// 将文件指针调整到起始处
				writer.truncate(0);
				siteFileWriter = writer;
			}, fileFail);
		}, fileFail);
	}

	var fileFail = function(error) {
		alert(error.code);
	}

	function createSiteFile() {
		start_gather = true;
		// 创建文件，以 时间 为文件名
		// window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, getRootName,
		// fileFail);
		getSiteFile();
	}

	function renameSiteFile(filename) {
		// siteFileName = filename;
		if (start_gather) {
			start_gather = false;
			siteFile.moveTo(dirSite, filename, success, fileFail);
			siteBakFileWriter.truncate(0);
		} else {
			siteBakFile.moveTo(dirSite, filename, success, fileFail);
			getBakSiteFile();
		}
	}

	function success() {
		// alert("保存成功。")
	}

	function saveBsInfor(bs_his) {
		var tempCid = (bs_his.cid & 0xffff);

		siteFileWriter.write(bs_his.lac + ' , ' + bs_his.cid + ' , ' + tempCid
				+ ' , ' + bs_his.time.toISOString() + "\n");
	}

	function saveCdmaInfor(bs_his) {
		siteFileWriter.write(bs_his.sid + ',' + bs_his.nid + ',' + bs_his.cid
				+ ',' + bs_his.time.toISOString() + ',' + bs_his.lat + ','
				+ bs_his.long + '\n');
	}

	function has_started() {
		return start_gather;
	}

	// 从 文件中获取 回放 列表
	function _getTraceList(file) {
		var reader = new FileReader();
		// 锁定基站表
		reader.onloadend = function(evt) {
			// console.log("-----lock file result--------"+reader.result);

			var lines = reader.result.split("\n");
			console.log("-----lines --------" + lines.length);
			for ( var i = 0; i < lines.length; i++) {
				var values = lines[i].split(",");
				// console.log("------values-----" + values.length);
				if (values.length < 3)
					break; // 跳过空行

				var lineObj = {}
				lineObj.lac = parseInt(values[0]);
				lineObj.cid = parseInt(values[1]);
				lineObj.time = parseInt(values[2]);
				playback_list.push(lineObj);
				// console.log("-----lock file lock_list --------" +
				// lineObj.time);
			}
		};
		reader.readAsText(file);
	}

	function getPlaybackList() {
		return playback_list;
	}

	function getFileList(callback) {
		console.log("-----getFileList--------" + dirSite.name);
		// Get a directory reader
		var dirSiteReader = dirSite.createReader();

		// Get a list of all the entries in the directory
		dirSiteReader.readEntries(callback, fileFail);
	}

	function setShowFile(file) {
		console.log("-----setShowFile-i-------" + i);
		showFile = file;
		console.log("-----setShowFile showFile--------" + showFile.name);
		_getTraceList(file);
	}

	function remove(file) {
		// remove this directory
		file.remove(successRemove, fileFail);
	}

	function successRemove(entry) {
		console.log("Removal succeeded");
	}

	fileOp.siteFileWriter = siteFileWriter;
	fileOp.createSiteFile = createSiteFile;
	fileOp.renameSiteFile = renameSiteFile;
	fileOp.has_started = has_started;
	fileOp.saveBsInfor = saveBsInfor;
	fileOp.saveCdmaInfor = saveCdmaInfor;
	fileOp.getFileList = getFileList;
	fileOp.setShowFile = setShowFile;
	fileOp.getPlaybackList = getPlaybackList;
	fileOp.remove = remove;

})();
