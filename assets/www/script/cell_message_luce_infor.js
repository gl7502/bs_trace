var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.CELL_MESSAGE_LUCE = {};

(function() {
	var lock_manage = hetulab.bstrace.lock_manager;
	var fileOp = hetulab.bstrace.fileoperate;
	var internet = hetulab.bstrace.internet;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;
	
	var operatorName,operator;
	var mcc,mnc;
	var bs_list = new Array();
//	var position = 0;
	
	var locations = new Array();
	var bs_list_position = 0;
	
	var start_gather; //true 开始采集；false 结束采集
	
	
	document.addEventListener('deviceready', function() {
		var wait = setInterval(function() {
//			_getOperatorName();
			_getCellInfor();	
			if(internet.hasNetWork()){
				updateLocations();
			}
		}, 2000);	
	},true);
	
//	var _getOperatorName = function() {
//		window.plugins.CellMessage.getOperatorName(
//		// success callback
//		function(result) {
//			var op = result.split(",")
//			operatorName = op[0];
//			operator = op[1];
////			console.log("-----operator and name ----" + operatorName + "," + operator);
//			return operatorName;
//		}
//		// failure callback,
//		, function(err) {
//			operatorName = err;
//			operator="";
//		});
//	}
	
//	function getOperatorName(){
//		return operatorName;
//	}
	
//	function getMcc(){
//		mcc = parseInt(operator.substring(0, 3));
//		return mcc
//	}
	
//	function getMnc(){
//	    mnc = parseInt(operator.substring(3));
//	    return mnc
//	}
	
	var bs_new = {
			operatorname: null,
			net : 0,
			lac : 0,
			cid : 0,
			psc : -1,
			rssi : 0,
			time : 0
		};
	
	// 最近的历史基站
	var bs_his = {
		operatorname : null,
		net : 0,
		lac : 0,
		cid : 0,
		psc : -1,
		rssi : 0,
		time : 0
	};
		
	var _getCellInfor = function() {		
		window.plugins.CellMessage.getCurBs(
		// success callback
		function(result) {
			var value = result.split(",");

			if (value.length >= 5) {
				bs_new.operatorname = value[0];
				bs_new.net = value[1];
				bs_new.lac = value[3];
				bs_new.cid = value[2];
				bs_new.psc = value[4];
				bs_new.rssi = value[5];
				
				_checkBsData();
			}
		}
		// failure callback,
		, function(err) {
			alert("err");
		});
		
		return bs_new;
	}
	
	function _checkBsData() {
		if(bs_new.lac == undefined) return;
		
		if (bs_his.lac == 0 || bs_his.cid == 0) {
			// 对于第一次采集到数据
			// 保存到历史记录中
			bs_new.time = new Date();
			bs_his.operatorname = bs_new.operatorname;
			bs_his.net = bs_new.net;
			bs_his.lac = bs_new.lac;
			bs_his.cid = bs_new.cid;
			bs_his.psc = bs_new.psc;
			bs_his.rssi = bs_new.rssi;
			bs_his.time = bs_new.time;
			// 检测 进入锁 告警
			 lockInAlertMsg(bs_new.lac,bs_new.cid);
			 addBs2ShowList(bs_new,bs_list);
//			 console.log("-----cell message push ---");
			// 保存基站历史信息时间 lac cid
			fileOp.saveBsInfor(bs_his);
		} else if (!(bs_new.lac == bs_his.lac && bs_new.cid == bs_his.cid)) {
			bs_new.time = new Date();
			// //检测进入锁告警
			 lockInAlertMsg(bs_new.lac,bs_new.cid);
			// //检测 离开锁 告警
			 lockOutAlertMsg(bs_his.lac,bs_his.cid);
			// 此时 bs 与历史记录不一样
			// 更改历史记录为当前值
			 bs_his.operatorname = bs_new.operatorname; 
			bs_his.net = bs_new.net;
			bs_his.lac = bs_new.lac;
			bs_his.cid = bs_new.cid;
			bs_his.psc = bs_new.psc;
			bs_his.rssi = bs_new.rssi;
			bs_his.time = bs_new.time;
			addBs2ShowList(bs_new,bs_list);
//			console.log("-----cell message push ---");
			// 保存基站历史信息时间 lac cid
			fileOp.saveBsInfor(bs_his);
		} else {
			// 此处 仅作为 测试，正常是没有这段
		}
	}
	
	//将基站添加到 用于显示的数组中；该数组要求，基站不能重复，新基站在数据的最后（未来显示的最前）
	function addBs2ShowList(newBs,showlist){
		for(var i=0;i < showlist.length; i++){
			if(showlist[i].lac == newBs.lac && showlist[i].cid == newBs.cid){
				//如果新基站原来在列表中，则删除老记录，增加新记录
				showlist.splice(i,1);
				showlist.push(newBs);
				return;
			}
		}
		//如果原来不存在，就直接添加新记录
		showlist.push(newBs);
	}
	
	// 进入锁 告警
	function lockInAlertMsg(lac, cid) {
		var lockStatus = lock_manage.getInLockStatus(lac, cid);
		function alertCallback() {
		}
		if (lockStatus) {
			navigator.notification.alert("您已进入" + lac + "-" + cid + "基站覆盖区",
					alertCallback, "进入锁告警", "知道了");
			navigator.notification.vibrate(2500);
			if(lock_manage.getAlertStatus(lac,cid)){
				navigator.notification.beep(2);				
			}
		}
	}
	// 离开锁告警
	function lockOutAlertMsg(lac, cid) {
		var lockStatus = lock_manage.getOutLockStatus(lac, cid);
		function alertCallback() {

		}
		if (lockStatus) {
			navigator.notification.alert("您已离开" + lac + "-" + cid + "基站覆盖区",
					alertCallback, "离开锁告警", "知道了");
			navigator.notification.vibrate(2500);			
			if(lock_manage.getAlertStatus(lac,cid)){
				navigator.notification.beep(2);
			}
		}
	}
	
	function getBsList(){
		return bs_list;
	}
	
	function getSigStren(){
		return bs_new.rssi;
	}
	
	function getOperatorName(){
		return bs_new.operatorname;
	}
	
	function getNetType(){
		return getNetTypeStr(bs_new.net);
	}
	
	function updateLocations(){

		var length = bs_list.length;
		
//		console.log("----updateLocations-----");
		
		if(length == 0) return;
		
//		console.log("----updateLocationsn-bs list length----" + length);		
//		console.log("----updateLocationsn-bs_list[i]----" + bs_list[bs_list_position]);

		for ( var i = bs_list_position; i < length; i++) {
//			console.log("----updateLocationsn-bs_list_position----" + bs_list_position);
//			console.log("----updateLocationsn-bs_list[0]----" + bs_list[0].lac);
//			console.log("----updateLocationsn-bs_list[bs_list_position].lac----" + bs_list[bs_list_position].lac);
			setTimeout(askLocation(bs_list[i].lac, bs_list[i].cid), (i-bs_list_position) * 200);	
		}
		bs_list_position = length;
	}
	
	function askLocation(lac,cid) {
		accessor.getLocation(lac,cid, accessCallback);
      }
	
	function accessCallback(tLac,tCid,tLoc) {
		var location = {};
		location.lac = tLac;
		location.cid = tCid;
		location.latitude = tLoc.location.latitude;
		location.longitude = tLoc.location.longitude;
		location.altitude = tLoc.location.altitude;
		location.accuracy = tLoc.location.accuracy;
		location.altitude_accuracy = tLoc.location.altitude_accuracy;
		location.street_number = tLoc.location.address.street_number;
		location.street = tLoc.location.address.street;
		location.postal_code = tLoc.location.address.postal_code;
		location.city = tLoc.location.address.city;
		location.county = tLoc.location.address.county;
		location.region = tLoc.location.address.region;
		location.country = tLoc.location.address.country;
		location.country_code = tLoc.location.address.country_code;		
		locations.push(location);
//		console.log("-------map control accessCallback push----");
	}
	
	function getLocations(position){
		var len = locations.length;
//		console.log("-------map control locations length----" + len);
//		console.log("-------map control locations position----" + position);
		if(len != 0 && len >= position){
			return locations.slice(position);
		}else{
			return null;
		}
	}

	
	function getNetTypeStr(type) {
		var netType;
		switch (type) {
		case 0:
			netType = "NETWORK_TYPE_UNKNOWN";
			break;
		case 1:
			netType = "NETWORK_TYPE_GPRS";
			break;
		case 2:
			netType = "NETWORK_TYPE_EDGE";
			break;
		case 3:
			netType = "NETWORK_TYPE_UMTS";
			break;
		case 8:
			netType = "NETWORK_TYPE_HSDPA";
			break;
		case 9:
			netType = "NETWORK_TYPE_HSUPA";
			break;
		case 10:
			netType = "NETWORK_TYPE_HSPA";
			break;
		case 4:
			netType = "NETWORK_TYPE_CDMA";
			break;
		case 5:
			netType = "NETWORK_TYPE_EVDO_0";
			break;
		case 6:
			netType = "NETWORK_TYPE_EVDO_A";
			break;
		case 12:
			netType = "NETWORK_TYPE_EVDO_B";
			break;
		case 7:
			netType = "NETWORK_TYPE_1xRTT";
			break;
		case 11:
			netType = "NETWORK_TYPE_IDEN";
			break;
		case 13:
			netType = "NETWORK_TYPE_LTE";
			break;
		case 14:
			netType = "NETWORK_TYPE_EHRPD";
			break;
		case 15:
			netType = "NETWORK_TYPE_HSPAP";
			break;
		default:
			netType = "ERR";
			break;
		}
		return netType;
	}
	
	var ns = hetulab.bstrace.CELL_MESSAGE_INFOR;

	ns.getOperatorName = getOperatorName;
	ns.getNetType = getNetType;
	ns.getBsList = getBsList;
	ns.getSigStren = getSigStren;
	ns.getLocations = getLocations;
	
})()