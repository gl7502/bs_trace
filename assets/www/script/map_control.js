var hetulab;
if (!hetulab)
	hetulab = {};
if (!hetulab.bstrace)
	hetulab.bstrace = {};
hetulab.bstrace.MAPCONTROL = {};

(function(){
	var CELL_MESSAGE_INFOR = hetulab.bstrace.CELL_MESSAGE_INFOR;
	var accessor = hetulab.bstrace.DATA_ACCESSOR;
	var ns = hetulab.bstrace.MAPCONTROL;
	
	var currentLocation = {};
	var curPos = {};
	
	function setCurLoc(tLac,tCid,tLoc){
		currentLocation.lac = tLac;
		currentLocation.cid = tCid;
		currentLocation.latitude = tLoc.location.latitude;
		currentLocation.longitude = tLoc.location.longitude;
		currentLocation.altitude = tLoc.location.altitude;
		currentLocation.accuracy = tLoc.location.accuracy;
		currentLocation.altitude_accuracy = tLoc.location.altitude_accuracy;
		currentLocation.street_number = tLoc.location.address.street_number;
		currentLocation.street = tLoc.location.address.street;
		currentLocation.postal_code = tLoc.location.address.postal_code;
		currentLocation.city = tLoc.location.address.city;
		currentLocation.county = tLoc.location.address.county;
		currentLocation.region = tLoc.location.address.region;
		currentLocation.country = tLoc.location.address.country;
		currentLocation.country_code = tLoc.location.address.country_code;
	}
	
	function getCurLoc(){
		return currentLocation;
	}
		
	function setPosition(position){
    	console.log("---map control setPosition-------" + position.coords.latitude);

		curPos = position;
		
    	console.log("---map control setPosition-------" + curPos.coords.latitude);

	}
	
	function getPosition(){
		return curPos;
	}
	
	ns.setCurLoc = setCurLoc;
	ns.getCurLoc = getCurLoc;
	ns.setPosition = setPosition;
	ns.getPosition = getPosition;
	
})()