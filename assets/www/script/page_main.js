
(function(){
//	var fileOp = hetulab.bstrace.fileoperate;
	
	document.addEventListener('deviceready', function() {

//		document.addEventListener("pause", function(){
//			navigator.app.exitApp();
//		}, false);	
		
		$("#bs_trace").click(function(){
//			console.log("------page change--bs_trace--------")
			 $.mobile.changePage($("#page_bs_list"),{ transition: "slide"});
		});
		
		$("#lock_manage").click(function(){
//			console.log("------page change--lock_manage--------")

			 $.mobile.changePage($("#page_lock_manage"),{ transition: "slide"});
		});
		
		$("#bs_search").click(function(){
//			console.log("------page change--bs_search--------")

			 $.mobile.changePage($("#page_search_location_input"),{ transition: "slide"});
		});
		
		$("#shortmessage").click(function(){
			console.log("------send--shortmessage--------")
			window.plugins.webintent.startActivity(
					{
						action : "com.hetulab.bstrace.SMS"					
				    },
					// success callback
					function(result) {

					}
					// failure callback,
					, function(err) {

					});
		});
		
		$("#data_manager").click(function(){
			//未加滑动效果，因为  加了之后显示效果不好。第一页会 回显。
			$.mobile.changePage($("#page_data_manager"),{ transition: "slide"});
		});
		
		$("#sys_exit").click(function(){
			showConfirm();
		});
		
		document.addEventListener("backbutton", onBackKeyDown, false);

	}, true);
	
	// callback function
	function onConfirm(button) {		     
	    // if press 'Yes'
	    if (button === 1){
	       navigator.app.exitApp();
	    }
	}
	 
	// PhoneGap Notification 提供的 Confirm API
	function showConfirm() {
	    navigator.notification.confirm(
	        '真的要退出基站定位系统吗?',  // message
	        onConfirm,            // callback function
	        '退出系统确认',               // title
	        '是的,取消'              // confirm 選項，用逗號隔開
	    );
	}
	
	$(document).on("pageshow", "#page_main", function() {
		document.addEventListener("backbutton", onBackKeyDown, false);			
	});		
	
	$(document).on("pagebeforehide", "#page_main", function(evt,ui) {
		document.removeEventListener("backbutton", onBackKeyDown, false);
	});
	
	function onBackKeyDown(){
//		alert("检测到返回键");
		showConfirm();
	}
	
})();
