package com.hetulab.bstrace;

import android.os.Bundle;
import org.apache.cordova.*;

public class Bs_traceActivity extends DroidGap {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setIntegerProperty("loadUrlTimeoutValue", 60000);
        super.loadUrl("file:///android_asset/www/bstracer.html");
    }
}