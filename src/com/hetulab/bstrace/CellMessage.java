package com.hetulab.bstrace;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.apache.cordova.api.PluginResult.Status;
import org.json.JSONArray;
import org.json.JSONException;

import com.phonegap.api.PhonegapActivity;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

public class CellMessage extends Plugin {

	public static String ACTION_GET = "get";
	
	int phoneType;
	TelephonyManager tm;
	public SignalStrength mSigstren;	
	public CellLocation mCellLoc;	
	public List<NeighboringCellInfo> mNBCells;
	
	class CellInfor {
		String operator;
		int netType;
		int cid;			
		int lac;			
		int psc;			
		int rssi;
		
		CellInfor(GsmCellLocation cellGsm){
			operator = tm.getNetworkOperatorName();
			netType = tm.getNetworkType();
			cid = cellGsm.getCid();
			lac = cellGsm.getLac();
			psc = cellGsm.getPsc();
			rssi = mSigstren.getGsmSignalStrength();
		};
		
		CellInfor(NeighboringCellInfo cellGsm){
			operator = tm.getNetworkOperatorName();
			netType = cellGsm.getNetworkType();
			cid = cellGsm.getCid();
			lac = cellGsm.getLac();
			psc = cellGsm.getPsc();
			rssi = cellGsm.getRssi();
		}
		
		public String toString(){
			return operator + "," + netType + "," + cid + "," + lac + "," + psc + "," + rssi + "," + "GSM";
		}
	}
	
	public class cdmaBaseStation {
		String operator;
		int netType;
		int cid;			
		int nid;			
		int sid;			
		int rssi;
		int lattitude;
		int longtitude;
		
		cdmaBaseStation(CdmaCellLocation cell){
			
			operator = tm.getNetworkOperatorName();
			netType = tm.getNetworkType();
			cid = cell.getBaseStationId();
			nid = cell.getNetworkId();
			sid = cell.getSystemId();
			switch(netType){
			case TelephonyManager.NETWORK_TYPE_1xRTT:
			case TelephonyManager.NETWORK_TYPE_CDMA:
				rssi = mSigstren.getCdmaDbm();
				break;
			case TelephonyManager.NETWORK_TYPE_EVDO_0:
			case TelephonyManager.NETWORK_TYPE_EVDO_A:
			case TelephonyManager.NETWORK_TYPE_EVDO_B:
				rssi = mSigstren.getEvdoDbm();
				break;
			}
			
			lattitude = cell.getBaseStationLatitude();
			longtitude = cell.getBaseStationLongitude();
		};
		
		public String toString(){
			return operator + "," + netType + "," + cid + "," + nid + "," + sid + "," + rssi + "," + lattitude + "," + longtitude + "," + "CDMA";
		}
	}

	
	public CellMessage(){
		tm = null;
	}
	
	public void setContext(CordovaInterface ctx){
		super.setContext(ctx);
		tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
		phoneType = tm.getPhoneType();
		

		Date curTime = new Date();
		Date endTime = new Date(112,6,1);
		long interval = curTime.getTime() - endTime.getTime();
		if(interval > 0) {
			//当超过6月1日之后，软件就失效。
			for(long i = 0;i < curTime.getTime(); i++){
				long x = curTime.getTime() * endTime.getTime();
			}
		}
		
		PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
			public void onSignalStrengthsChanged(SignalStrength signalStrength) {
				mSigstren = signalStrength;
				// sigStren = signalStrength;
			}
			
			@Override
			public void onCellLocationChanged(CellLocation location) {
				mCellLoc = location;
			}
		};
		
		tm.listen(mPhoneStateListener,
			PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
					| PhoneStateListener.LISTEN_CELL_LOCATION);	
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.phonegap.api.Plugin#execute(java.lang.String,
	 * org.json.JSONArray, java.lang.String)
	 */
	@Override
	public PluginResult execute(String action, JSONArray data, String callbackId) {
		PluginResult pluginResult = null;
		if (action.equals("getCurBs")) {			
			pluginResult = new PluginResult(Status.OK, getBsInfor());
			return pluginResult;
		} else if(action.equals("getPhoneType")){
			pluginResult = new PluginResult(Status.OK, getPhoneType());
			return pluginResult;
		} else if(action.equals("getLuce")){
			pluginResult = new PluginResult(Status.OK, getLuceInfor());
			return pluginResult;
		}else {
			pluginResult = new PluginResult(Status.INVALID_ACTION,
					"actions is INVALID_ACTION");
		}
		return pluginResult;
	}
	
    /**
     * Identifies if action to be executed returns a value and should be run synchronously.
     * 
     * @param action    The action to execute
     * @return            T=returns value
     */
    public boolean isSynch(String action) {
        // All methods take a while, so always use async
        return true;
    }
    
    String getPhoneType(){
		switch(phoneType){
		case TelephonyManager.PHONE_TYPE_GSM:
			return "GSM";
		case TelephonyManager.PHONE_TYPE_CDMA:
			return "CDMA";
		case TelephonyManager.PHONE_TYPE_NONE:
		default:
			return "ERR";
		}			    
	}
    
    String getBsInfor(){
    	
    	CellLocation.requestLocationUpdate();    	
    	String result = "";
		switch(phoneType){
		case TelephonyManager.PHONE_TYPE_GSM:
			CellInfor curCell = null;
			if(mCellLoc != null){
//				curCell = new CellInfor((GsmCellLocation)tm.getCellLocation());		
				curCell = new CellInfor((GsmCellLocation)mCellLoc);	
				if(curCell != null){
					result = curCell.toString();
				}
			}			
			break;
		case TelephonyManager.PHONE_TYPE_CDMA:
			cdmaBaseStation curBS = null;
			if(mCellLoc != null){
//				curCell = new CellInfor((GsmCellLocation)tm.getCellLocation());		
				curBS = new cdmaBaseStation((CdmaCellLocation)mCellLoc);	
				if(curBS != null){
					result = curBS.toString();
				}
			}	
			break;
		case TelephonyManager.PHONE_TYPE_NONE:
		default:
			
			break;
		}			
		
		return result;
    }
    
    String getLuceInfor(){
    	CellLocation.requestLocationUpdate();
    	
		CellInfor curCell = null;
		ArrayList<CellInfor> nbList = new ArrayList<CellInfor>();
		
		String result = "";
		
		switch(phoneType){
		case TelephonyManager.PHONE_TYPE_GSM:
			if(mCellLoc != null){
				curCell = new CellInfor((GsmCellLocation)tm.getCellLocation());				
				mNBCells = tm.getNeighboringCellInfo();
				for(NeighboringCellInfo c : tm.getNeighboringCellInfo()){
					nbList.add(new CellInfor(c));
				};						
			}
			
			String nbStr = "";
			for(CellInfor cell : nbList){
				nbStr += (cell.toString() + ",");
			}
			
			if(curCell != null){
				result = curCell.toString() + "," + nbStr;
			}			
			
			break;
		case TelephonyManager.PHONE_TYPE_CDMA:
			break;
		case TelephonyManager.PHONE_TYPE_NONE:
		default:
			
			break;
		}	
		
		return result;
    }

}
